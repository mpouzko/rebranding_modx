<?php 
Class Templater {
    private $path;
    
    public function __construct($path = null){
        $this->path = $path ?:$_SERVER["DOCUMENT_ROOT"];
        if (substr($this->path,0,-1) !== '/') 
            $this->path .= "/";
    }

    public function getChunk($chunkName, $params=[]) {
        $filename = $this->path . $chunkName;
        $chunkContents = file_get_contents($filename);
        if (count ($params) > 0) {
            foreach ($params as $key => $value) {
                $chunkContents = str_replace(
                    "{{{$key}}}",
                    $value,
                    $chunkContents
                );
 
            }
        }
        return $chunkContents;
    }
}