<?php
include(__DIR__.'/lib/kp/templater.php');
$chunkPath = dirname(__DIR__).'/src/chunks';
$templater = new Templater($chunkPath);

// compile chunks 
$time=time();
$output = ''
. $templater->getChunk('_head.html', [
    "test_css" => "<link rel=\"stylesheet\" href=\"./css/test_css_kp.css?time={$time}\"/>",
    "test_js" => "<script src=\"./js/1.js?time={$time}\"></script>",
    "time" => $time,    
])
. $templater->getChunk(
    '_body.html', 
    [
        "header"=> $templater->getChunk(
            'header.html',
            [
                "logo" => "media/logo_ru.svg",
                "offer_image" => "assets/img/dummy.jpg"
            ]
            ),
            "video" => $templater->getChunk('video.html'),
            "book_form" => $templater->getChunk('book_form.html'),
            "offers" => $templater->getChunk('offers.html'),
            "special-event" => $templater->getChunk('special-event.html'),
            "summer-activities" => $templater->getChunk('summer-activities.html'),
            "events" => $templater->getChunk('events.html'),
            "winter" => $templater->getChunk('winter.html'),
            "app" => $templater->getChunk('app.html'),
            "subscribe" => $templater->getChunk('subscribe.html'),
            "blog" => $templater->getChunk('blog.html'),
            "popup" => $templater->getChunk('popup.html'),
            "footer"=> $templater->getChunk(
                'footer.html',
                [
                    "logo" => "media/logo_footer_ru.svg",
                    "phone" => "8 800 550 20 20",
                    "email" => "infocenter@krasnayapolyanaresort.ru",
                    "google_play" => "google.play",
                    "appstore" => "app.store",
                    "menu" => $templater->getChunk("footer_menu.html")
                ]
            )
    ]
)
 
; 

print("Build completed\n");

// Output results
file_put_contents(dirname(__DIR__)."/public/index.html",$output);
