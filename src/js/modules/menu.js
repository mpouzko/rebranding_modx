domLoad(()=>{
    const   MENU_SW_SELECTOR = 'js-kp-menu-sw',
            MENU_SELECTOR = 'js-kp-menu',
            MENU_SW_OPEN_CLASS = 'header__menu-switch--open',
            MENU_OPEN_CLASS = 'header__controls--open';
    const   menuSw = document.getElementsByClassName(MENU_SW_SELECTOR)[0];
    const   menu = document.getElementsByClassName(MENU_SELECTOR)[0];
    
    if (!menuSw) return;
    menuSw.addEventListener('click',(evt)=>{
          evt.target.classList.toggle(MENU_SW_OPEN_CLASS);
          if (evt.target.classList.contains(MENU_SW_OPEN_CLASS)) {
              menu.classList.add(MENU_OPEN_CLASS);
          } else {
              menu.classList.remove(MENU_OPEN_CLASS);
          }
    })
})