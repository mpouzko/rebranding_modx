domLoad(()=> {
    const  getOffset = (el) => {
        var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }
    


    const   BOOK_FORM = "js-kp-book-form",
            TAB_SELECTOR = ".book-form__tab",
            HEADER_SELECTOR = ".book-form__tab-header",
            WRAPPER_SELECTOR = ".book-form__tab-wrapper",
            WRAPPER_COLLAPSED_CLASS="book-form__tab-wrapper--collapsed",
            ACTIVE_TAB_CLASS = "book-form__tab--active",
            ACTIVE_HEADER_CLASS = "book-form__tab-header--active";

    const forms = document.getElementsByClassName(BOOK_FORM);
    
    iterate( forms, (_,form) => {
        let headers = form.querySelectorAll(HEADER_SELECTOR);
        let tabs = form.querySelectorAll(TAB_SELECTOR);
        let wrapper = form.querySelector(WRAPPER_SELECTOR);

        iterate(headers,(i,h)=>{
            h.addEventListener('click',(e)=>{

                log(h);
                
                if (h.tagName.toLowerCase() == 'a') {
                    return true;
                }
                wrapper.classList.add(WRAPPER_COLLAPSED_CLASS);

                setTimeout(() => {
                    iterate(headers,(_,hdr)=>{
                        hdr.classList.remove(ACTIVE_HEADER_CLASS);
                    });
                    iterate(tabs,(_,tab)=>{
                        tab.classList.remove(ACTIVE_TAB_CLASS);
                    });
                    h.classList.add(ACTIVE_HEADER_CLASS);
                    tabs[i].classList.add(ACTIVE_TAB_CLASS);
                    setTimeout(() => {
                        wrapper.classList.remove(WRAPPER_COLLAPSED_CLASS);
                        setTimeout(() => {
                            let offsetY = getOffset(h).top;
                            console.log(offsetY, window.scrollY, window.innerHeight);
                            if (
                                offsetY <= window.scrollY + window.innerHeight
                                &&
                                offsetY >= window.scrollY
                                ) {
                                return; }
                            window.scrollTo({
                            top: offsetY - window.innerHeight / 2,
                                    behavior: "smooth"
                            });

                            }, 300);
                    },800)
                }, 500);
                
            })
        })

    })
})