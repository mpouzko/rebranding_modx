domLoad( () => {
    const   PANEL_SELECTOR = ".js-kp-home-panel",
            PANEL_SWITCH = "js-ctrl",
            COLLAPSE_CLASS = "home-panel--collapsed";

    const items = document.getElementsByClassName(PANEL_SWITCH);
        iterate(items, (_,v) => {
            v.addEventListener('click', (e) => {
                e.preventDefault();
                let parent = v.closest(PANEL_SELECTOR);
                log(parent);
                if (parent) {
                    parent.classList.toggle(COLLAPSE_CLASS);
                }
            })


        })
    }
)