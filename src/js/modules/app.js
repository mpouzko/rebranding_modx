// @koala-append "jquery.touchSwipe.min.js"
// @koala-append "jquery.visible.min.js"
// @koala-append "pickmeup.js"


// @koala-append "helper.js"

// @koala-append "ajax-form.js"

// @koala-append "gallery.js"
// @koala-append "gallery-basic.js"
// @koala-append "gallery-thumbs.js"

// @koala-append "tables.js"
// @koala-append "tabs.js"

// @koala-append "webcams.js"
// @koala-append "moreitems.js"

// @koala-append "dropdown.js"
// @koala-append "datepicker.js"
// @koala-append "accordion.js"

// @koala-append "resource-gallery.js"
// @koala-append "book-form.js"

// @koala-append "travelline-form.js"
// @koala-append "book-form-hotel.js"
// @koala-append "guest.js"
// @koala-append "mega-filter.js"
// @koala-append "events-calendar.js"
// @koala-append "scrollable.js"
// @koala-append "menu-mobile.js"
// @koala-append "menu.js"
// @koala-append "header.js"
// @koala-append "resort-map.js"
// @koala-append "info-map.js"
// @koala-append "category-filter.js"
// @koala-append "offer.js"
// @koala-append "offer-container.js"
// @koala-append "weather.js"
// @koala-append "panel.js"
// @koala-append "shops-grid.js"
// @koala-append "chase-panel.js"
// @koala-append "modal.js"
// @koala-append "slopes-map.js"
// @koala-append "jquery.vide.min.js"
// @koala-append "ajax-data.js"
// @koala-append "home-panel.js"
// @koala-append "search.js"
// @koala-append "ecommerce.js"


if ( typeof(jQuery) == undefined ) {
	var script = document.createElement("script");
	script.src = "/assets/template/pm/js/jquery-3.0.0.min.js";
	document.head.appendChild(script);
}


(function($){

//scrolltop button
$(".controls.scroll-top").click(function(){
    $('html,body').animate({ scrollTop: 0 }, 1000);
})
//close button
$(".js-close-switch").click(function(event) {
	$(this).parent().fadeOut();
});


})(jQuery);