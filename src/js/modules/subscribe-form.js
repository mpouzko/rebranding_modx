domLoad(()=> {

    const   FORM_CLASS = "js-kp-subscribe-form",
            FORM_SUCCESS = "home-subscribe--success";

    const forms = document.getElementsByClassName(FORM_CLASS);
    
    function process_result(response,form) {
        
        if (response.success) {
            form.classList.add(FORM_SUCCESS);
            form.innerText = "Вы успешно подписались на новости!"
        } else {
            alert("Проверьте введенный e-mail");
        }
    }

    iterate( forms, (_,form) => {
  
        form.addEventListener('submit',(e)=>{
            e.preventDefault();
            const spinner = new Spinner(form);
            const xhr = new XMLHttpRequest();
            const formData = new FormData(form);
            const buttons = form.getElementsByTagName("button");
            iterate(buttons,(_,btn)=> {
                btn.disabled = true;
            })
            xhr.open(form.method.toUpperCase(), form.action, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('soc-fetch-mode', 'cors');
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(formData);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    spinner.remove();
                    iterate(buttons,(_,btn)=> {
                        btn.disabled = false;
                    });
                    if (xhr.status === 200) {
                        process_result(xhr.responseText, form);
                        
                    } else {
                        alert('Произошла ошибка.' + xhr.responseText);
                    }

                }
                  
        };
            
        })

    });
})