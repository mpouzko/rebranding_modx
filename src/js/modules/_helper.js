const domLoad = (callback) => {
    if (typeof(callback) !== 'function' && typeof(callback) !== 'string') {
        throw `type of callback must be a string or function`;
    }
    document.addEventListener('DOMContentLoaded', ()=>{
        
            typeof(callback) === 'function' ?
                callback.call(window) :
                window[callback]
    })

}

const iterate = (collection,callback) => {
    for (let i = 0; i<collection.length;i++) 
        callback(i,collection[i]);
}

const log = (msg) => {
    if (window.__KP_debug) {
        console.log(msg);
    }
}

function get_url_param(name) {
    var _url = new URL(location.href);
   var _params = new URLSearchParams(_url.search);
   var _result = _params.get(name) || false;
   return _result;
}

function clear_url_params(url){
   return url.replace(/\?.*$/g,"");
}
function remove_location(url) {
   return url.replace(/^.*\?+/g,'');
}