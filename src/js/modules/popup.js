domLoad(function () {
    window.addEventListener('scroll', function () {
        var POPUP_SELECTOR = "js-popup",
            STICKY_CLASS = "kp-popup--fixed";
        var popup = document.getElementsByClassName(POPUP_SELECTOR)[0];
        if (!popup) return;
        if (pageYOffset === 0) {
            popup.classList.remove(STICKY_CLASS);
        } else {
            popup.classList.add(STICKY_CLASS);
        }
    }); //Trigger immediately after page load 

    if (pageYOffset === 0) {
        var e = new Event('scroll');
        window.dispatchEvent(e);
    }
    $(".js-popup").click(function () {
        $(this).remove();
        
    })
})