class Timer {
    constructor (element) {
        this.element;
        this.deadline;
        this.interval;
        let timer = element.dataset.timer;
        if (!timer) return null;
        this.element = element;
        this.deadline = element.dataset.timer*1000;
        let final = timer*1000;
        let now = new Date().getTime();
        var total = final - now;

        if (total <= 0) {
            element.innerText = "00:00:00";
            return this;
        } 
        
        if (total > (86400000)) {
            const days = Math.floor(total / (86400000));
            element.innerText = `${days} дней`;
            return;
        }

        this.interval = setInterval(() => {
            this.refresh();
        }, 1000);
        
        return this;
    }

    refresh(){
        let now = new Date().getTime();
        let t = this.deadline - now;
        
        if (t<=0) {
            this.stop();
        } 
        let seconds = Math.floor( (t/1000) % 60 );
        let minutes = Math.floor( (t/1000/60) % 60 );
        let hours = Math.floor( (t/(1000*60*60)) % 24 );
        let days = Math.floor( t/(1000*60*60*24) );
        let elements = [hours,minutes,seconds].map((e)=>{return parseInt(e)<10?"0"+e:e});
        this.element.innerText = elements.join(":");
    }

    stop(){
        clearInterval(this.interval);
    }

}