class Spinner 
{
    constructor(element) {
        let position = getComputedStyle(element).position;

        if (position === "" || position==="static") {
            element.style.position = "relative";
        }
        let spinner = element.querySelector(".spinner");
        if (spinner) {
            this.element = element;
            return this;
        };
        spinner = document.createElement("div");
        spinner.classList.add("spinner");
        element.appendChild(spinner);
        for (let i = 1 ;i<4;i++) {
            let div = document.createElement("div");
            div.classList.add("bounce"+i);
            spinner.appendChild(div);
        }
        this.element = element;
        return this;
    }

    remove(element) {
        if (!element){ 
            element = this.element;
        }
        let spinner = element.querySelector(".spinner");
        if (spinner) {
            element.removeChild(spinner);
        }
    }

}