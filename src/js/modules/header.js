domLoad( () => {
    window.addEventListener('scroll', () =>  {

        const   HEADER_SELECTOR="js-kp-header",
                STICKY_CLASS = "header--fixed";

        const header = document.getElementsByClassName(HEADER_SELECTOR)[0];
        
        if (pageYOffset === 0) {
            header.classList.remove(STICKY_CLASS)
        } else {
            header.classList.add(STICKY_CLASS)
        }
        
    });

    //Trigger immediately after page load 
    if (pageYOffset === 0) {
        const e = new Event ('scroll');
        window.dispatchEvent(e);
    }

})