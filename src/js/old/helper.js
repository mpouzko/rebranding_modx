//helper functions
function get_url_param(name) {
 	var _url = new URL(location.href);
    var _params = new URLSearchParams(_url.search);
    var _result = _params.get(name) || false;
    return _result;
}

function clear_url_params(url){
	return url.replace(/\?.*$/g,"");
}
function remove_location(url) {
	return url.replace(/^.*\?+/g,'');
}

function preload(url, callback) {
        	var _image = $("<img class=\"rg__preload\" />").appendTo(document.body);
            _image.attr("src", url);
            _image.one("load", function() {
	            if (typeof callback == "function") 
	            	callback();
				_image.remove();
			})
			if(_image[0].complete) $(_image).trigger('load');
			
        }
