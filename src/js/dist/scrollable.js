 $(document).ready(function(){
        const SELECTOR = ".js-scrollable",
              SELECTOR_ITEM = ".js-scrollable__item",
              SELECTOR_PAGER = ".js-scrollable__page",
              INDICATOR_SELECTOR = ".js-indicator",
              INDICATOR_ITEM_ACTIVE_CLASS = "scrollable__indicator-item--active";

        var defaults = {},
            options = {},
            _indicatorTpl = "<div class=\"scrollable__indicator js-indicator\"></div>",
            _indicatorItemTpl = "<div class=\"scrollable__indicator-item\"></div>";
        
        var methods = {
            init:function(params) {
                options = $.extend({}, defaults, params);
                var 
                    page_left = $("<div></div>").addClass("scrollable__page scrollable__page-left").appendTo(this),
                    page_right = $("<div></div>").addClass("scrollable__page scrollable__page-right").appendTo(this),
                    self = this;
                    this.css("position","relative");
                $(_indicatorTpl).appendTo(self);
                $(self).scrollable("show");
                var interval = $(self).scrollable("newInterval");
                //controls
                $(page_left).click(function(event) {
                     $(self).scrollable("show", -1);
                     clearInterval(interval); 
                    interval = $(self).scrollable("newInterval");                
                });
                $(page_right).click(function(event) {
                     $(self).scrollable("show", 1);
                     clearInterval(interval); 
                     interval = $(self).scrollable("newInterval");                
                });
                this.mouseenter(function(){
                    clearInterval(interval);
                });
                this.mouseleave(function(){
                    interval = $(self).scrollable("newInterval");                
                })
                this.swipe( {
                    swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
                        clearInterval(interval); 
                        event.preventDefault();
                        $(self).scrollable("show", -1);    
                        
                    },
                    swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
                        event.preventDefault();
                        $(self).scrollable("show", 1);
                        clearInterval(interval); 
                        interval = $(self).scrollable("newInterval");                
                    },
                    threshold:75
                  });
                //resize
                $(window).on('load resize', function(event) {
                    $(self).scrollable("show");
                });
               
                
                return this;
            },
            newInterval:function(){
                var self = this;
                if ( parseInt( $(self).data("timer") ) > 200 ) {
                    return setInterval(
                            function(){
                                $(self).scrollable("show", 1);      
                            }, 
                            parseInt( $(self).data("timer") )
                        );
                }
                return false;

            },
            itemsPerView:function() {
                return Math.round( $(this).width() / ($(this).find(SELECTOR_ITEM+":eq(0)").innerWidth() ) ) || 1;
                
            },
            show:function(direction = null) {
                
                var offset = parseInt($(this).attr("data-offset")) || 0,
                    child = $(this).find(`${SELECTOR_ITEM}:visible`).slice(0,1),
                    items_per_view = parseInt($(this).scrollable("itemsPerView")),
                    total = parseInt($(this).find(SELECTOR_ITEM).length);
                var visible = $(this).find(`${SELECTOR_ITEM}:visible`);
                
                
                //$(this).css("height", $(child).find("*:lt(1)").prop("scrollHeight"));
                
                if (total <= items_per_view) {
                    $(this).find(SELECTOR_PAGER).hide();
                    $(this).find(SELECTOR_ITEM).fadeIn();    
                    return this;
                }

                $(this).find(SELECTOR_PAGER).show();
                var selected = $(this).find(SELECTOR_ITEM).slice(0,items_per_view);
                if (direction == 1) {
                    var newOffset = offset + items_per_view > total - items_per_view ? total  -  items_per_view : offset + items_per_view;
                    var selected =  $(this).find(`${SELECTOR_ITEM}:gt(${newOffset-1})`).slice(0,items_per_view);
                    if ( offset + items_per_view  == total) {
                        selected =  $(this).find(SELECTOR_ITEM).slice(0,items_per_view);
                        newOffset = 0;
                    }
                }
                else if (direction == -1) {
                    if (offset == 0) {
                        var newOffset = total - items_per_view;
                    }
                    else {
                      var newOffset = offset <= items_per_view ? 0 : offset - items_per_view ;
                    }

                    var _selector = (newOffset == 0 ) ? (SELECTOR_ITEM ):( `${SELECTOR_ITEM}:gt(${newOffset-1})` );
                        selected =  $(this).find(_selector).slice(0,items_per_view);
                }
                
                $(visible).finish().fadeOut(600,"linear",function(){
                    $(selected).fadeIn(1200,"linear");     

                });
                
                $(this).attr("data-offset",newOffset);
                
                $(this).scrollable("update_indicator", newOffset);
                return this; 
            },
            update_indicator : function( newOffset = 0){
                
                var indicator = $(this).find(INDICATOR_SELECTOR),
                    items_per_view = parseInt($(this).scrollable("itemsPerView")),
                    total = parseInt($(this).find(SELECTOR_ITEM).length);
                if ( !$(this).data("indicator")  || ( total <= items_per_view ) ) {
                    $(indicator).hide();
                    return this;
                }
                else {
                    $(indicator).show();
                }
                var num_pages = Math.ceil(total / items_per_view);
                var num_items = $(indicator).children().length;
                if (num_items <  num_pages ) {
                    for (var i = 0; i < (num_pages - num_items); i++) {
                        var item = $(_indicatorItemTpl).appendTo(indicator);
                    }    
                }
                if (num_items >  num_pages ) {
                    $(indicator).children("*:gt("+ (num_pages -1) +")").remove();
                }
                //var active_page = Math.ceil(newOffset /  total);
                //console.log(newOffset, total, active_page);
                $(indicator).children().removeClass(INDICATOR_ITEM_ACTIVE_CLASS);
                $(indicator).children().eq( newOffset).addClass(INDICATOR_ITEM_ACTIVE_CLASS);

            }
            

        };
        $.fn.scrollable = function(method){
            if ( methods[method] ) {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply( this, arguments );
            } else {
                $.error( 'method "' +  method + '" not found' );
            }
        };

        //init 
        $(SELECTOR).each(function(index, el) {
            $(el).scrollable();
        });
        
        }
    )
