"use strict";

domLoad(function () {
  var getOffset = function getOffset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
      top: rect.top + scrollTop,
      left: rect.left + scrollLeft
    };
  };

  var BOOK_FORM = "js-kp-book-form",
      TAB_SELECTOR = ".book-form__tab",
      HEADER_SELECTOR = ".book-form__tab-header",
      WRAPPER_SELECTOR = ".book-form__tab-wrapper",
      WRAPPER_COLLAPSED_CLASS = "book-form__tab-wrapper--collapsed",
      ACTIVE_TAB_CLASS = "book-form__tab--active",
      ACTIVE_HEADER_CLASS = "book-form__tab-header--active";
  var forms = document.getElementsByClassName(BOOK_FORM);
  iterate(forms, function (_, form) {
    var headers = form.querySelectorAll(HEADER_SELECTOR);
    var tabs = form.querySelectorAll(TAB_SELECTOR);
    var wrapper = form.querySelector(WRAPPER_SELECTOR);
    iterate(headers, function (i, h) {
      h.addEventListener('click', function (e) {
        log(h);

        if (h.tagName.toLowerCase() == 'a') {
          return true;
        }

        wrapper.classList.add(WRAPPER_COLLAPSED_CLASS);
        setTimeout(function () {
          iterate(headers, function (_, hdr) {
            hdr.classList.remove(ACTIVE_HEADER_CLASS);
          });
          iterate(tabs, function (_, tab) {
            tab.classList.remove(ACTIVE_TAB_CLASS);
          });
          h.classList.add(ACTIVE_HEADER_CLASS);
          tabs[i].classList.add(ACTIVE_TAB_CLASS);
          setTimeout(function () {
            wrapper.classList.remove(WRAPPER_COLLAPSED_CLASS);
            setTimeout(function () {
              var offsetY = getOffset(h).top;
              console.log(offsetY, window.scrollY, window.innerHeight);

              if (offsetY <= window.scrollY + window.innerHeight && offsetY >= window.scrollY) {
                return;
              }

              window.scrollTo({
                top: offsetY - window.innerHeight / 2,
                behavior: "smooth"
              });
            }, 300);
          }, 800);
        }, 500);
      });
    });
  });
});