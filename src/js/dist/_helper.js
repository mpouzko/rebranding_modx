"use strict";

var domLoad = function domLoad(callback) {
  if (typeof callback !== 'function' && typeof callback !== 'string') {
    throw "type of callback must be a string or function";
  }

  document.addEventListener('DOMContentLoaded', function () {
    typeof callback === 'function' ? callback.call(window) : window[callback];
  });
};

var iterate = function iterate(collection, callback) {
  for (var i = 0; i < collection.length; i++) {
    callback(i, collection[i]);
  }
};

var log = function log(msg) {
  if (window.__KP_debug) {
    console.log(msg);
  }
};

function get_url_param(name) {
  var _url = new URL(location.href);

  var _params = new URLSearchParams(_url.search);

  var _result = _params.get(name) || false;

  return _result;
}

function clear_url_params(url) {
  return url.replace(/\?.*$/g, "");
}

function remove_location(url) {
  return url.replace(/^.*\?+/g, '');
}