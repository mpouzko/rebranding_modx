$(document).ready(function(){
    const SELECTOR = ".js-ajax-element",
          SCROLLABLE_CLASS = "scrollable",
          TIMER_SELECTOR = ".js-timer";
          

    var defaults = {},
        options = {};
        
    
    var methods = {
        pre:function(params) {
            var self = this;
            if ($(self).data("url")) {
                $(self).ajaxElement("loadItems");
            }
            return this;
        },
        init:function(html){
            var self = this;
            $(self).html(html);
            if ($(self).data("scrollable")) {
                $(self).addClass(SCROLLABLE_CLASS);
                $(self).scrollable();
            }
            if ($(self).find(TIMER_SELECTOR).length >0 ) {
                $(self).ajaxElement("initTimers");
            }
            return this;

        },
        loadItems: function(){
            var spinner = new Spinner(this[0]);
            var self = this;
            $.ajax({
                type: "GET",
                dataType:'text/html',
                url: $(this).data("url"),
                cache:false,
                complete: function(response) {
                    if (response.status === 200) {
                        spinner.remove();
                        
                        $(self).ajaxElement("init",response.responseText)
                    } else {

                        $(self).ajaxElement("handleError");
                    }
                }
            });
            return this;
        },
        handleError:function(){
            var self =this;
            setTimeout(function() {
                $(self).ajaxElement();
            }, 10000);
            return this;
        },

        initTimers: function(){
            var timers = $(this).find(".js-timer");
            $(timers).each(function(_,e) {
                new Timer(e);
            })
        }

    };
    $.fn.ajaxElement = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.pre.apply( this, arguments );
        } else {
            $.error( 'method "' +  method + '" not found' );
        }
    };

    //init 
    $(SELECTOR).each(function(index, el) {
        $(el).ajaxElement();
    });
    
    }
)
