"use strict";

domLoad(function () {
  var PANEL_SELECTOR = ".js-kp-home-panel",
      PANEL_SWITCH = "js-ctrl",
      COLLAPSE_CLASS = "home-panel--collapsed";
  var items = document.getElementsByClassName(PANEL_SWITCH);
  iterate(items, function (_, v) {
    v.addEventListener('click', function (e) {
      e.preventDefault();
      var parent = v.closest(PANEL_SELECTOR);
      log(parent);

      if (parent) {
        parent.classList.toggle(COLLAPSE_CLASS);
      }
    });
  });
});