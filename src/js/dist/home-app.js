$(document).ready(function(){
    var HAND_SELECTOR = ".js-hand",
        VISIBLE_CLASS = "home-app__hand--visible";
        function check_visibility() {
            var hand = $(HAND_SELECTOR);
            if ($ (hand).visible()) {
                $(hand).addClass(VISIBLE_CLASS);
            } else {
                $(hand).removeClass(VISIBLE_CLASS);
            }
        }
    $(window).scroll(check_visibility);
    check_visibility();
})