"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Timer =
/*#__PURE__*/
function () {
  function Timer(element) {
    var _this = this;

    _classCallCheck(this, Timer);

    this.element;
    this.deadline;
    this.interval;
    var timer = element.dataset.timer;
    if (!timer) return null;
    this.element = element;
    this.deadline = element.dataset.timer * 1000;

    var _final = timer * 1000;

    var now = new Date().getTime();
    var total = _final - now;

    if (total <= 0) {
      element.innerText = "00:00:00";
      return this;
    }

    if (total > 86400000) {
      var days = Math.floor(total / 86400000);
      element.innerText = "".concat(days, " \u0434\u043D\u0435\u0439");
      return;
    }

    this.interval = setInterval(function () {
      _this.refresh();
    }, 1000);
    return this;
  }

  _createClass(Timer, [{
    key: "refresh",
    value: function refresh() {
      var now = new Date().getTime();
      var t = this.deadline - now;

      if (t <= 0) {
        this.stop();
      }

      var seconds = Math.floor(t / 1000 % 60);
      var minutes = Math.floor(t / 1000 / 60 % 60);
      var hours = Math.floor(t / (1000 * 60 * 60) % 24);
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      var elements = [hours, minutes, seconds].map(function (e) {
        return parseInt(e) < 10 ? "0" + e : e;
      });
      this.element.innerText = elements.join(":");
    }
  }, {
    key: "stop",
    value: function stop() {
      clearInterval(this.interval);
    }
  }]);

  return Timer;
}();