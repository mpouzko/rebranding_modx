(function($){
	$(document).ready(function(e){
		var value,
			URL_PARAM_CLASS     = "js-url-param",
			GROUP_SELECTOR      = ".js-guests__group",
			PLUS_SELECTOR       = ".js-plus",
			VALUE_SELECTOR      = ".js-value",
			MINUS_SELECTOR      = ".js-minus",
			CAPTION_SELECTOR    = ".js-caption",
			PLACEHOLDER_CLASS	= "guests__value--placeholder",
			PANEL_SELECTOR      = ".js-panel",
			CAPTION_HASVAL = "guests__caption--has-value",
			PANEL_VISIBLE_CLASS = "guests__panel--visible",
			_tpl = {
		    		1:'<a class="guests__minus js-minus">–</a>\
		    		<a class="guests__plus js-plus">+</a>\
		    		<p class="guests__value %placeholder_class% js-value ">%placeholder%<span>%postfix%</span></p>',

		    		2:'<div class="guests__panel js-panel">\
						<a class="guests__minus js-minus">–</a>\
						<a class="guests__plus js-plus">+</a>\
						<p class="guests__value js-value guests__value--init">%placeholder%<span>%postfix%</span></p>\
		    		</div>'
		    },
		    locale = {
		    	children: {
			    	ru: {
			    		0:  "детей",
			    		1:  "ребенок",
			    		4:  "ребенка",
			    		99: "детей", 

			    	},
			    	en: {
			    		0:  "children",
			    		1:  "child",
			    		99: "children"

			    	}
			    }
		    },
		    	
			READY_CLASS = "guests--ready";
		    
		var methods = {
		    init:function(params) {
		    	var self = this;
		    	var lang = $("html").attr("lang") || "ru";
		    	var groups = $(this).find(GROUP_SELECTOR);
		    	$(groups).each(function(index, el) {
		    		$(self).guests("renderInput",index,el);
		    	});

		    	$(self).find(PLUS_SELECTOR).click(function(event) {
		    		var btn = this;
		    		if ( $(btn).parent().hasClass("js-panel") )
		    			btn = $(btn).parent();
		    		var input = $(btn).siblings("input")[0];
		    		var val = parseInt($(input).val());
		    		var max = parseInt($(input).attr("max")) || 10;
		    		var postfix = $(input).data("postfix");
		    		if (max > val) {
		    			++val;
		    			$(input).val(val);
		    			if ( $(btn).hasClass("js-panel") )
		    				var value = $(btn).children(VALUE_SELECTOR)[0]
		    			else
		    				var value = $(btn).siblings(VALUE_SELECTOR)[0];
		    			
		    			//console.log(postfix,locale,locale[postfix]);
		    			if ( typeof locale[postfix] !== "undefined") {
		    				$.each(locale[postfix][lang],function(index, el) {
		    					//console.log(index,el,val <= index);
		    					if (val <= index) {
		    						postfix = el;
		    						return false;
		    					}
		    				});
		    			}
		    			$(value).html(val + "<span>" + postfix + "</span>");
		    			$(value).removeClass(PLACEHOLDER_CLASS);
		    			if ( $(btn).hasClass("js-panel") ) {
		    				var caption = $(btn).siblings(CAPTION_SELECTOR);
		    				caption.addClass(CAPTION_HASVAL);
		    				caption.text("+" + val + " " + postfix);
		    			}
		    		}
		    	});
		    	$(self).find(MINUS_SELECTOR).click(function(event) {
		    		var btn = this;
		    		if ( $(btn).parent().hasClass("js-panel") )
		    			btn = $(btn).parent();
		    		var input = $(btn).siblings("input")[0];
		    		var val = parseInt($(input).val());

		    		var min = parseInt($(input).attr("min")) || 0;
		    		var postfix = $(input).data("postfix");
		    		//console.log(btn,input,val,min,postfix);
		    		if (min < val) {
		    			--val;
		    			$(input).val(val);
		    			if ( $(btn).hasClass("js-panel") )
		    				var value = $(btn).children(VALUE_SELECTOR)[0]
		    			else
		    				var value = $(btn).siblings(VALUE_SELECTOR)[0];
		    			if ( typeof locale[postfix] !== "undefined") {
		    				$.each(locale[postfix][lang],function(index, el) {
		    					if (val <= index) {
		    						postfix = el;
		    						return false;
		    					}
		    				});
		    			}
		    			$(value).html(val + "<span>" + postfix + "</span>");
		    			$(value).removeClass(PLACEHOLDER_CLASS);
		    			if ( $(btn).hasClass("js-panel") ) {
		    				var caption = $(btn).siblings(CAPTION_SELECTOR);
		    				caption.addClass(CAPTION_HASVAL);
		    				caption.text("+" + val + " " + postfix);
		    			}

		    		}
		    	});
		    	$(self).find(CAPTION_SELECTOR).click(function(event) {
		    		event.stopPropagation();
		    		var caption = this;
		    		var panel = $(caption).siblings(PANEL_SELECTOR);
		    		var parent = $(panel).closest(".booking-form__item")[0];
		    		var left = "0rem";
		    		var width = $(parent).width();
		    		var top = $(caption).position().top - 5;
		    		$(document).on('click',function(e){
		    			// if the target of the click isn't the container nor a descendant of the container
		    			if (!panel.is(e.target) && panel.has(e.target).length === 0) 
		    			    {
		    			        $(panel).removeClass(PANEL_VISIBLE_CLASS);
		    			        $(document).off('click',panel);
		    			    }
		    		});

		    		$(panel).css({
		    			left:left,
		    			width: width+"px"
		    		});
		    		$(panel).addClass(PANEL_VISIBLE_CLASS);
		    	});
		    
		    	$(self).addClass(READY_CLASS);
		    	return this;
		    },
		    renderInput: function(index = 0, element) {
		    	var lang = $("html").attr("lang") || "ru" ;
		    	var input = $(element).find("input")[0];
		    	var config = {
		    		mode : $(input).data("mode") || ((++index)%2 ),
		    		placeholder :  $(input).val() ||$(input).data("placeholder") || "",
		    		placeholder_class : $(input).data("placeholder") == undefined ? "" : PLACEHOLDER_CLASS,
		    		caption :  $(input).data("caption") || "",
		    		postfix :  $(input).data("postfix") || "",
		    		min :  $(input).attr("min") || 0,
		    		max :  $(input).attr("max") || 10,
		    		value :  $(input).val() || $(input).attr("value") || 0,
		    	};
		    	if ( typeof locale[config.postfix] !== "undefined") {
		    		$.each(locale[config.postfix][lang],function(index, el) {
		    			if (config.value <= index) {
		    				config.postfix = el;
		    				return false;
		    			}
		    		});
		    	}
		    	var html = _tpl[config.mode];
		    	for (key in config) html = html.replace( "%"+key+"%", config[key]);
		    	if ( $(input).val() && config.mode == 2 ) {
		    			var val = parseInt($(input).val());
		    			var min = parseInt($(input).attr("min")) || 0;
		    			var value = $(element).children(VALUE_SELECTOR)[0]
		    			var postfix = $(input).data("postfix");
		    			if (min < val) {
		    				$(input).val(val);
		    				if ( typeof locale[postfix] !== "undefined") {
		    					$.each(locale[postfix][lang],function(_index, _el) {
		    						if (val <= _index) {
		    							postfix = _el;
		    							return false;
		    						}
		    					});
		    				}
		    				$(value).html(val + "<span>" + postfix + "</span>");
		    				var caption = $(element).find(CAPTION_SELECTOR);
		    				caption.addClass(CAPTION_HASVAL);
		    				caption.text("+" + val + " " + postfix);
		    			}
		    	}
		    	$(element).append($(html));
		    	$(input).hide();
		    }
		};
		$.fn.guests = function(method){
		    if ( methods[method] ) {
		        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		    } else if ( typeof method === 'object' || ! method ) {
		        return methods.init.apply( this, arguments );
		    } else {
		        $.error( 'method "' +  method + '" not found' );
		    }
		};
		$(".js-guests").each(function(index, el) {
			$(this).guests();			
		});


 		
 		}
	)
}
)(jQuery);