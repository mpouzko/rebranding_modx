(function($){
	$(document).ready(function(e){
	
		var translations=[];
		var locale;
		translations['en'] = {
			months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			today: 'Today '
		};
		translations['ru'] = {
			months: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
			today: 'Сегодня '
		};

        var URL_PARAM_CLASS = "js-url-param";
        
        var PMU_WIDTH = 337;

		var methods = {
		    init:function(params) {

		    	locale = $("html").attr("lang") || 'ru';
		    	var self = this;
		    	
		    	$(this).hide();
		    	var season = $(this).data("season") || "winter";
		    	id = 0;
		    	while ($("#datepicker-"+id).length >0 ) {
		    		id++;
		    	}

		    	this.attr("id","datepicker-input-"+id);	
		    	//id = "datepicker-"+id;   
		    	var select = $("<div></div>").append("<div class=\"caption datepicker__caption\"></div><div class=\"value datepicker__value\"><table><tr><td class=\"day\" rowspan=\"2\"></td><td class=\"month\"></td></tr><tr><td class=\"year\"></td></tr></table></div>").addClass( $(this).attr("class") ).attr({"id":"datepicker-"+id, "data-input":"#datepicker-input-"+id});
				var date = new Date;		
				if ($(this).attr("data-date") && $(this).attr("data-date").length>0) {
					var increment = parseInt($(this).attr("data-date").replace(/[^\d]*/,""));
					date.setDate(date.getDate() + increment);		
				}

				if ($(this).attr("placeholder")) {
					var _ph = $(this).attr("placeholder").split(/[\.\,\;\-\:]/);
					    	$(select).find(".value .day").text(_ph[0]);
							$(select).find(".value .year").text(_ph[2]);
							$(select).find(".value .month").html(_ph[1]);
							$(select).find(".value").addClass("datepicker__value--placeholder");
				}
				else {
					$(select).datePicker("setDate",date, self);		
				}

				var param = get_url_param($(this).attr("name"));
				if ($(this).hasClass(URL_PARAM_CLASS) && param) {
					//console.log(param);
					param = param.replace(/(-([1..9]){1}-)/, '-0$2-');
					var _ph = param.split(/[\.\,\;\-\:]/);
					date.setDate(_ph[2]);
					date.setMonth(parseInt(_ph[1]) -1 );
					date.setYear(_ph[0]);
					$(select).datePicker("setDate",date, self);		
				}
				if ($(this).attr("data-locale") && pickmeup.defaults.locales.hasOwnProperty($(this).attr("data-locale"))) {
					locale = $(this).attr("data-locale");
				}
		    	$(select).insertAfter(this);
		        pickmeup("#datepicker-"+id,{
		    		locale:locale,
		    		next:'<i class="fa fa-angle-right"></i>',
		    		prev:'<i class="fa fa-angle-left"></i>',
		    		hide_on_select:true,
		    		position:function(){
		    			var element = this;
		    			//window.e = this;
		    			var index = $(element).parent().index();
		    			//console.log(index);
		    			if (index == 0 ) {
			    			/* var parent = $(element).closest(".book-form"); */
			    			var left = $(element).offset().left;
		    			}
		    			else {
			    			var left = parseInt($(element).parent().offset().left) ;	
                        }
                        
                        var width = window.innerWidth;
                        left = left + PMU_WIDTH > width ? width - PMU_WIDTH : left;
		    			var top = parseInt($(element).offset().top) + parseInt( $(element).height());
		    			
		    			return {
		    				top:top + "px",
		    				left:left  + "px"
		    			}
		    		},
		    		min: (new Date),
		    		format	: 'd-m-Y'
                });
                select[0].addEventListener('pickmeup-hide', function (e) {
                    $(select).children(".caption").removeClass("datepicker__caption--open");
                    $(select).children(".value").removeClass("datepicker__value--open");  
                })
                select[0].addEventListener('pickmeup-show', function (e) {
                    $(select).children(".caption").addClass("datepicker__caption--open");
                    $(select).children(".value").addClass("datepicker__value--open");  
                })

		    	$(select).children(".caption").text( $(this).attr("data-caption") );
		  
                
		    	select[0].addEventListener('pickmeup-change', function (e) {
		    		var input = $($(e.target).attr("data-input"));
                    $(select).datePicker("setDate",e.detail.date, input );
                    $(select).children(".caption").removeClass("datepicker__caption--open");
                    $(select).children(".value").removeClass("datepicker__value--open");
		    		
				});
				
		    	return this;
		    },
		    setDate:function(date, obj) {
		    	var _date = parseInt(date.getDate());
		    	_date = _date > 9 ? _date : "0" + _date;
		    	var _year = date.getFullYear();
		    	var _month = date.getMonth();
		    	var formatted_date = _year + "-" + (_month+1) + "-" + _date;
		    	$(this).find(".value .day").text(_date);
				$(this).find(".value .year").text(_year);
				$(this).find(".value .month").html( translations[locale].months[_month] );
				$(this).find(".value").removeClass("datepicker__value--placeholder");
				$(this).find(".caption").removeClass("datepicker__caption--open");
				obj.val(formatted_date);

				return this;
		    },
/*		    getTodayText:function(){
		    	var locale = $("html").attr("lang") || 'ru';
		    	var date = new Date();
		    	var _date = parseInt(date.getDate());
		    	var _month = date.getMonth();
		    	var formatted_date = translations[locale].today +_date + " " + translations[locale].months[_month];
		    	return formatted_date;

		    }*/
		    
		    
		};
		$.fn.datePicker = function(method){
		    if ( methods[method] ) {
		        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		    } else if ( typeof method === 'object' || ! method ) {
		        return methods.init.apply( this, arguments );
		    } else {
		        $.error( 'method "' +  method + '" not found' );
		    }
		};

		$(".js-datepicker").each(function(){
			$(this).datePicker();	
 		});
 		/*var _html = "<div class='pmu-goto-today'>" + $().datePicker("getTodayText") + "</div>";
		$(".pickmeup").append(_html);*/



/* 		function get_url_param(name) {
 			var _url = new URL(location.href);
            var _params = new URLSearchParams(_url.search);
            var _result = _params.get(name) || false;
            return _result;
 		}*/


 		}
	)
}
)(jQuery);