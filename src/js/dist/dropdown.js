(function($){
	$(document).ready(function(e){
		var value,
			URL_PARAM_CLASS = "js-url-param";
		var methods = {
		    init:function(params) {
		    	var self = this;
		    	var season = $(this).data("season") || "winter";
		    	var select = $("<div></div>").append("<div class=\"caption dropdown__caption  dropdown__caption--"+season+"\"></div><div class=\"value dropdown__value\"></div>").addClass( $(this).attr("class") );
		    	var list = $("<ul class=\"list dropdown__list\"></ul>").appendTo($("body")).hide();
		    	var postfix = "";
		    	if (typeof $(this).data("postfix") != "undefined") {
		    		postfix = "<span>" + $(this).data("postfix") + "</span>";
		    	}
		    	//console.log(typeof $(this).data("postfix"), $(this).data("postfix"), postfix);

		    	
		    	$(this).hide();
		    	$(this).children("option").each(function(index, el) {
		    		if (index==0) {
		    			$(select).children(".value").text( $(el).text()).attr("data-val", $(el).attr("value") );
		    		}
		    		var _li = "<li class=\"dropdown__item\" data-val=\"" + $(el).attr("value") + "\" data-index=\"" + index +"\">" + $(el).text()+ "</li>";
		    		$(list).append( _li );
		    	});    	

		    	if ($(this).attr("min") && $(this).attr("max")) {
		    		for (var i = parseInt($(this).attr("min")); i <=  parseInt($(this).attr("max")); i++) {
		    			var _li = "<li class=\"dropdown__item\" data-val=\"" + i + "\">" + i + "</li>";
		    			$(list).append( _li );	
		    		}
		    	}

		    	$(select).insertAfter(this);

		    	if ($(this).attr("value") ) {
		    		value = $(this).attr("value");
		    		$(select).find(".value").attr("data-val",value).html(value +postfix );	
		    	}

		    	if ($(this).attr("data-placeholder") ) {
		    		$(select).find(".value").addClass("dropdown__value--placeholder");
		    	}
		    	
		    	$(select).children(".caption").text( $(this).attr("data-caption") );
		    	$(select).children().click(function(event) {
		    		event.stopPropagation();
		    		$(this).toggleClass("dropdown__caption--open");
		    		list.css({
			    		top: $(this).offset().top +32+ "px",
			    		left: $(this).offset().left + "px"
		    		});
		    		$("body > *").not(list).on("click.list", function(){
		    			list.toggle();
		    			$("body > *").unbind(".list");
		    		});
		    		list.toggle();
		    	});
				
		    	$(list).children().click(function(event) {
		    		var index = $(event.target).attr("data-index") ? $(event.target).attr("data-index"):false;
		    		$("body > *").unbind(".list");
		    		$(select).find(".caption").toggleClass("dropdown__caption--open");
		    		$(self).dropDown( "change", $(event.target).attr("data-val"), $(event.target).text(), select,list,index, postfix);
		    	});
		    	var param = get_url_param($(this).attr("name"));
		    	if ( $(this).hasClass(URL_PARAM_CLASS) && param ) {
		    		$(list).children().filter(function(){
		    			return $(this).data("val") == param;
		    		})
		    		.trigger("click");
					
		    	}
		    	return this;
		    },
		    change:function(value_, text, object,listObject,index, postfix = "") {
		    	value = value_;
		    	object.find(".value").attr("data-val",value).html(text +  postfix ).removeClass("dropdown__value--placeholder");
		    	listObject.hide();
		    	if (object.hasClass("dropdown-inline")) {
		    		object.find(".caption").text( this.attr("data-caption")+ ": " + text);
		    	}
		    	if (index) {
		    		this[0].selectedIndex = index;
		    	}
		    	else this[0].value = value;
		    	return this;
		    },
		    getValue:function() {
		    	return value;
		    },
		    
		};
		$.fn.dropDown = function(method){
		    if ( methods[method] ) {
		        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		    } else if ( typeof method === 'object' || ! method ) {
		        return methods.init.apply( this, arguments );
		    } else {
		        $.error( 'method "' +  method + '" not found' );
		    }
		};
		$(".js-dropdown").each(function(index, el) {
			$(this).dropDown();			
		});


 		
 		}
	)
}
)(jQuery);