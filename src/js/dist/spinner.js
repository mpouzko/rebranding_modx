"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Spinner =
/*#__PURE__*/
function () {
  function Spinner(element) {
    _classCallCheck(this, Spinner);

    var position = getComputedStyle(element).position;

    if (position === "" || position === "static") {
      element.style.position = "relative";
    }

    var spinner = element.querySelector(".spinner");

    if (spinner) {
      this.element = element;
      return this;
    }

    ;
    spinner = document.createElement("div");
    spinner.classList.add("spinner");
    element.appendChild(spinner);

    for (var i = 1; i < 4; i++) {
      var div = document.createElement("div");
      div.classList.add("bounce" + i);
      spinner.appendChild(div);
    }

    this.element = element;
    return this;
  }

  _createClass(Spinner, [{
    key: "remove",
    value: function remove(element) {
      if (!element) {
        element = this.element;
      }

      var spinner = element.querySelector(".spinner");

      if (spinner) {
        element.removeChild(spinner);
      }
    }
  }]);

  return Spinner;
}();