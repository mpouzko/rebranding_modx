"use strict";

domLoad(function () {
  window.addEventListener('scroll', function () {
    var HEADER_SELECTOR = "js-kp-header",
        STICKY_CLASS = "header--fixed";
    var header = document.getElementsByClassName(HEADER_SELECTOR)[0];

    if (pageYOffset === 0) {
      header.classList.remove(STICKY_CLASS);
    } else {
      header.classList.add(STICKY_CLASS);
    }
  }); //Trigger immediately after page load 

  if (pageYOffset === 0) {
    var e = new Event('scroll');
    window.dispatchEvent(e);
  }
});