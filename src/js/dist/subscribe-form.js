"use strict";

domLoad(function () {
  var FORM_CLASS = "js-kp-subscribe-form",
      FORM_SUCCESS = "home-subscribe--success";
  var forms = document.getElementsByClassName(FORM_CLASS);

  function process_result(response, form) {
    if (response.success) {
      form.classList.add(FORM_SUCCESS);
      form.innerText = "Вы успешно подписались на новости!";
    } else {
      alert("Проверьте введенный e-mail");
    }
  }

  iterate(forms, function (_, form) {
    form.addEventListener('submit', function (e) {
      e.preventDefault();
      var spinner = new Spinner(form);
      var xhr = new XMLHttpRequest();
      var formData = new FormData(form);
      var buttons = form.getElementsByTagName("button");
      iterate(buttons, function (_, btn) {
        btn.disabled = true;
      });
      xhr.open(form.method.toUpperCase(), form.action, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.setRequestHeader('soc-fetch-mode', 'cors');
      xhr.setRequestHeader('Cache-Control', 'no-cache');
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.send(formData);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          spinner.remove();
          iterate(buttons, function (_, btn) {
            btn.disabled = false;
          });

          if (xhr.status === 200) {
            process_result(xhr.responseText, form);
          } else {
            alert('Произошла ошибка.' + xhr.responseText);
          }
        }
      };
    });
  });
});