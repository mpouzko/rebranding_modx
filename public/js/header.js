"use strict";

var domLoad = function domLoad(callback) {
  if (typeof callback !== 'function' && typeof callback !== 'string') {
    throw "type of callback must be a string or function";
  }

  document.addEventListener('DOMContentLoaded', function () {
    typeof callback === 'function' ? callback.call(window) : window[callback];
  });
};

var iterate = function iterate(collection, callback) {
  for (var i = 0; i < collection.length; i++) {
    callback(i, collection[i]);
  }
};

var log = function log(msg) {
  if (window.__KP_debug) {
    console.log(msg);
  }
};

function get_url_param(name) {
  var _url = new URL(location.href);

  var _params = new URLSearchParams(_url.search);

  var _result = _params.get(name) || false;

  return _result;
}

function clear_url_params(url) {
  return url.replace(/\?.*$/g, "");
}

function remove_location(url) {
  return url.replace(/^.*\?+/g, '');
}
"use strict";

domLoad(function () {
  window.addEventListener('scroll', function () {
    var HEADER_SELECTOR = "js-kp-header",
        STICKY_CLASS = "header--fixed";
    var header = document.getElementsByClassName(HEADER_SELECTOR)[0];

    if (pageYOffset === 0) {
      header.classList.remove(STICKY_CLASS);
    } else {
      header.classList.add(STICKY_CLASS);
    }
  }); //Trigger immediately after page load 

  if (pageYOffset === 0) {
    var e = new Event('scroll');
    window.dispatchEvent(e);
  }
});
"use strict";

domLoad(function () {
  var MENU_SW_SELECTOR = 'js-kp-menu-sw',
      MENU_SELECTOR = 'js-kp-menu',
      MENU_SW_OPEN_CLASS = 'header__menu-switch--open',
      MENU_OPEN_CLASS = 'header__controls--open';
  var menuSw = document.getElementsByClassName(MENU_SW_SELECTOR)[0];
  var menu = document.getElementsByClassName(MENU_SELECTOR)[0];
  if (!menuSw) return;
  menuSw.addEventListener('click', function (evt) {
    evt.target.classList.toggle(MENU_SW_OPEN_CLASS);

    if (evt.target.classList.contains(MENU_SW_OPEN_CLASS)) {
      menu.classList.add(MENU_OPEN_CLASS);
    } else {
      menu.classList.remove(MENU_OPEN_CLASS);
    }
  });
});
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Spinner =
/*#__PURE__*/
function () {
  function Spinner(element) {
    _classCallCheck(this, Spinner);

    var position = getComputedStyle(element).position;

    if (position === "" || position === "static") {
      element.style.position = "relative";
    }

    var spinner = element.querySelector(".spinner");

    if (spinner) {
      this.element = element;
      return this;
    }

    ;
    spinner = document.createElement("div");
    spinner.classList.add("spinner");
    element.appendChild(spinner);

    for (var i = 1; i < 4; i++) {
      var div = document.createElement("div");
      div.classList.add("bounce" + i);
      spinner.appendChild(div);
    }

    this.element = element;
    return this;
  }

  _createClass(Spinner, [{
    key: "remove",
    value: function remove(element) {
      if (!element) {
        element = this.element;
      }

      var spinner = element.querySelector(".spinner");

      if (spinner) {
        element.removeChild(spinner);
      }
    }
  }]);

  return Spinner;
}();
