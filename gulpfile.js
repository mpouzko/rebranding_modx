const gulp = require('gulp'),
      sass = require('gulp-sass'),
      minify = require('gulp-minify'),
      autoprefixer = require('gulp-autoprefixer'),
      concatCss = require('gulp-concat-css'),
      importCss = require('gulp-import-css'),
      cleanCSS = require('gulp-clean-css'),
      sassLint = require('gulp-sass-lint'),
      exec = require('child_process').exec,
      sourcemaps = require('gulp-sourcemaps'),
      babel = require('gulp-babel'),
      concat = require('gulp-concat'),
      include = require('gulp-include');

function all(callback) {
    build_html(callback);
    compile_css(callback);
    babel_js(callback);
    concat_js(callback);
    callback();
}
function compile_css(callback) {
    gulp.src("./src/css/*.scss", {debug:false})
        .pipe(sassLint({ 
            options: {
                formatter: 'stylish',
                'merge-default-rules': false,
            },
            files: {
                include: '**/*.scss'
            },
            rules: {
                'no-css-comments': true,
                'indentation':4
                }
            }
        ))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
        .pipe(sass({
            outputStyle: 'nested',
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            "overrideBrowserslist": ["> 1%"],
            "cascade": false
        }))
        .pipe(importCss())

        .pipe(cleanCSS({ compatibility: 'ie8' }))        

        .pipe(gulp.dest('./public/css'));
        

    callback();
}

function babel_js (callback) {
    
    gulp.src('./src/js/modules/*.js')
        .pipe(babel({ presets: ['@babel/env']}))
        .pipe(gulp.dest('./src/js/dist/'));
        callback();

}

function concat_js (callback) {
    gulp.src('./src/js/*.js')
        .pipe(include())
        .pipe(minify({
            ext:{
                min:'.min.js'
            }
        }))
            .on('error', console.log)
        .pipe(gulp.dest('./public/js'));
        callback();
}

function build_html(callback) {
    exec('php -f ./builder/build.php', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
      });
      callback();
}

gulp.watch('./src/chunks/*.html',build_html);
gulp.watch('./builder/**.*',build_html);
gulp.watch('./src/css/*.(css|scss)',compile_css);
gulp.watch('./src/css/chunks/*.(css|scss)',compile_css);
gulp.watch('./src/js/modules/*.js',babel_js);
gulp.watch('./src/js/dist/*.js',concat_js);
gulp.watch('./src/js/*.js',concat_js);

exports.default = all;